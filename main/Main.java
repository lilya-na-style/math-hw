package main;

public class Main {
    public static void main(String[] args) {
        double s = 0.0;

        MathEx ex = new MathEx();
        System.out.println(ex.countShellDistanceInRad(12.5,20.0, s));
        System.out.println(ex.countShellDistanceInDegrees(30, 60));
        System.out.println(ex.getCarsDistance(20,80, 50,2 ));

        System.out.println(ex.getExpressionForPoint(3,1.5));
        System.out.println(ex.countExpression(6.66));
        System.out.println(ex.countExpression(5.43));
    }
}
