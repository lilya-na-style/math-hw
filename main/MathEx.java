package main;

public class MathEx {

    //1 ex.
    //rad

    public double countShellDistanceInRad(double v, double a, double s){
      s = ((v * v) / 9.8) * Math.sin(Math.toRadians(2 * a));
        return s;

    }
    //degrees

    public double countShellDistanceInDegrees(double a, double v){
        double s = ((v * v) / 9.8) * (Math.sin(2 * a));
        return s;

    }

    //2 ex.
    public double getCarsDistance(double s, double v1, double v2, double time) {
        double finalS = s + v1 * time + v2 * time;
        return finalS;

    }


    //3 ex.

        public int getExpressionForPoint(double x, double y){

            int point;
            if(((x >= 0) && (y >= 1.5 * x - 1) && (y <= x)) || ((x <= 0) && (y >= -1.5 * x - 1) && (y <= -x))) {
                point = 1;

            } else {
                point = 0;

            }

            return point;

        }

    //4 ex.

    public double countExpression(double x){

        double i = Math.exp(x + 1) + 2 * Math.exp(x) * Math.cos(x);
        double j = x - Math.exp(x + 1) * Math.sin(x);
        double result = (6 * Math.log1p(Math.sqrt(i))) / (Math.log1p(j))
                + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return result;



    }
}
