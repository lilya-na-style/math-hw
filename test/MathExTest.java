package test;

import main.MathEx;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

public class MathExTest {

    MathEx cut = new MathEx();

    static Arguments[] countShellDistanceInRadArgs(){

        return new Arguments[]{
                Arguments.arguments(30.0, 50.0, 90.44152833785583),
                Arguments.arguments(12.5, 20.0, 10.24852694015528)
        };
    }

    @ParameterizedTest
    @MethodSource("countShellDistanceInRadArgs")
    void countShellDistanceInRadTest(double v, double a, double expected){
        double actual = cut.countShellDistanceInRad(v, a, expected);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getCarsDistanceArgs(){

        return new Arguments[]{
                Arguments.arguments(20.0, 80.0, 50.0, 2.0, 280.0),
        };
    }
    @ParameterizedTest
    @MethodSource("getCarsDistanceArgs")
    void getCarsDistanceTest(double v1, double v2, double s, double time,
                             double expected){
        double actual = cut.getCarsDistance(v1, v2, s, time);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getExpressionForPointArgs(){

        return new Arguments[]{
                Arguments.arguments(1.0, 0.0, 0),
                Arguments.arguments(3.0, -2.2, 0),
                Arguments.arguments(0.7, 0.1, 1),
                Arguments.arguments(-0.5, 2.5, 0)
        };
    }
    @ParameterizedTest
    @MethodSource("getExpressionForPointArgs")
    void getExpressionForPointTest(double x, double y, int expected){
        double actual = cut.getExpressionForPoint(x, y);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] countExpressionArgs(){

        return new Arguments[]{
                Arguments.arguments(6.66, Double.NaN),
                Arguments.arguments(5.43, 4.751831781510971)
        };
    }


    @ParameterizedTest
    @MethodSource("countExpressionArgs")
    void countExpressionTest(double x, double expected){
        double actual = cut.countExpression(x);
        Assertions.assertEquals(expected, actual);
    }




}
